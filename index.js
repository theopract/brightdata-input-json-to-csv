const dropZone = document.getElementById("drop_zone");

function saveFile(filename, data) {
    const blob = new Blob([data], {type: 'text/csv'});
    if (window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    } else {
        const elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;        
        document.body.appendChild(elem);
        elem.click();        
        document.body.removeChild(elem);
    }
}

async function handleDropZoneFileSelect(evt) {
    console.log('Processing new file...')
    evt.stopPropagation();
    evt.preventDefault();
    dropZone.classList.remove('drag-over-effect');

    const files = evt.dataTransfer.files; // FileList object.

    const output = [];

    function getData(file) {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();

        reader.onload = e => {
          resolve(reader.result);
        };

        reader.readAsText(file);
      });
    }

    function readFile(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
    
            reader.onload = e => {
              resolve(reader.result);
            };
    
            reader.readAsText(file);
          });
    }

    const file = files[0];
    const fileName = file.name.split('.')[0]+'.csv';
    console.log(`Reading file ${files[0]}`);
    const content = await readFile(files[0]).catch(err => console.warn(err));
    console.log(`Reading complete!`);

    const json = JSON.parse(content);

    const keys = Object.keys(json[0]);
    let csv = keys.join(',') + '\n';

    json.forEach((e, i) => {
        const output = keys.map(key => `"${JSON.stringify(e[key])?.replace(/\"/g, '""') || ''}"`).join(',') + '\n'
        // console.log({csvLength: csv.length, outputLength: output.length, index: i})
        csv += output;
    })
    
    saveFile(fileName, csv);

    console.groupCollapsed(
        "%c Первые 10 строк CSV: ",
        "background: green; color: white; font-weight: bold;"
      );
    console.log(csv.split('\n').slice(0, 10).join('\n'))
    console.groupEnd();
  }

function handleDropZoneDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = "copy"; // Explicitly show this is a copy.
    dropZone.classList.add('drag-over-effect');
}

function handleDropZoneDragLeave(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    dropZone.classList.remove('drag-over-effect');
}

window.onload = function () {
  dropZone.addEventListener("dragover", handleDropZoneDragOver, false);
  dropZone.addEventListener("drop", handleDropZoneFileSelect, false);
  dropZone.addEventListener("dragleave", handleDropZoneDragLeave);

};
