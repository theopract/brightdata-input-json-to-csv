import gulp from 'gulp';
import plumber from 'gulp-plumber';
import postcss from 'gulp-postcss';
import csso from 'postcss-csso';
import rename from 'gulp-rename';
import autoprefixer from 'autoprefixer';
import browser from 'browser-sync';
import htmlmin from 'gulp-htmlmin';
import terser from 'gulp-terser';
import del from 'del';

// Styles

export const styles = () => {
  return gulp.src('src/style.css', { sourcemaps: true })
    .pipe(plumber())
    .pipe(postcss([
      autoprefixer(),
      csso()
    ]))
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('build/css', { sourcemaps: '.' }))
    .pipe(browser.stream());
}

// Styles unminified

export const stylesUnminified = () => {
  return gulp.src('src/style.css', { sourcemaps: true })
    .pipe(plumber())
    .pipe(postcss([
      autoprefixer(),
    ]))
    .pipe(rename('style.css'))
    .pipe(gulp.dest('build/css', { sourcemaps: '.' }))
    .pipe(browser.stream());
}

// HTML

const html = () => {
  return gulp.src('./*.html')
  .pipe(htmlmin({ collapseWhitespace: true }))
  .pipe(gulp.dest('build'));
}

// Scripts

const scripts = () => {
  return gulp.src('index.js')
    .pipe(terser())
    .pipe(gulp.dest('build/js'));
}

// Clean

const clean = () => {
  return del('build');
}

// Server

const server = (done) => {
  browser.init({
    server: {
      baseDir: 'build'
    },
    cors: true,
    notify: false,
    ui: false,
  });
  done();
}

// Reload

const reload = (done) => {
  browser.reload();
  done();
}

// Watcher

const watcher = () => {
  gulp.watch('src/style.css', gulp.series(styles));
  gulp.watch('index.js', gulp.series(scripts));
  gulp.watch('index.html', gulp.series(html, reload));
}

//Build

export const build = gulp.series(
  clean,
  gulp.parallel(
    styles,
    stylesUnminified,
    html,
    scripts,
  ),
);

// Default

export default gulp.series(
  clean,
  gulp.parallel(
    styles,
    stylesUnminified,
    html,
    scripts
  ),
  gulp.series(
    server,
    watcher
  ));

